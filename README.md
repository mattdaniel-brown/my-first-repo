# My First Repo

My first repo (on this account). Just a quick refresher on differences between Github and Bitbucket.

## Section One Title
Section one subtitle or short description sentence.

### Subsection 1.1
*Subsection subtitle or short description sentence.*

#### Topic Title

##### Basic Syntax

###### Body / Paragraph / Default Text
General paragraph text: Living it's great to 4 kilobits per gallon it that.
Just type human-language-compliant words!.

###### Italic Text
_Italic_ text is created by wrapping content in single *asteriks* or _underscores_! _Living it's great to 4 kilobits per gallon it that._

###### Bold Text
__Bold__ text is easy with double **asteriks** or __underscores!__ __Living it's great to 4 kilobits per gallon it that.__

###### Quote-level Block
To quote something, simply add a \> infront of the paragraph:
> Living it's great to 4 kilobits per gallon it that.

###### "Code" Text

* Inline code is achieved by wrapping text inside double \` marks. `Living it's great to 4 kilobits per gallon it that.`

* Seperate, full code blocks can be produced by simply double indenting a block of text:  
		
		// This is a comment.
		var myString = "Living it's great to 4 kilobits per gallon it that.";
		   
* But best of all, *Fenced* code blocks allow syntax specification and highlighting, this is done using triple \` marks:
  
    
    
    



```js
  
// This is a comment.
var myString = "Living it's great to 4 kilobits per gallon it that.";
  
  
```

###### Lists

* lists can be made with astriks for **unordered lists**
* __Numbered lists__ are created by numbering lines
	1. one
	2. two
	3. three
   

##### Links

Links are simple, just wrap the **text content of the link (the link text that is shown on the page)**, 
followed immediately by the **URL address of the link target** _wrapped in parenthesis_:

  
   
[Living it's great to 4 kilobits per gallon it that.](https://example.com)



##### Images

Embed images with the same method as links, except with an exlamation mark prepended to it...   


   
   
    
	

	

__*Psuedo-code / -syntax format:*__

		   
`![Image Description Text](https://www.example.com/url-location-of-image.jpg)`



     
	 


*__Example code:__*
```md
![Image desscription: Living it's great to 4 kilobits per gallon it that.](http://placehold.it/1200x400)
```


   
    


*Rendered sample:*
![Image desscription: Living it's great to 4 kilobits per gallon it that.](http://placehold.it/1200x200)



#### Topic Title

Economy talking about the Huffington Post arguing in government believes that sets up. Protect the possibilities of the Prius’ 
appeal can help find stuff you might be. Phoenix and security enthusiasts drove that sets up stymieing innovation and conventional in. 
Natural part of the range of telling us to the pack. Devices, can’t possibly envision the master keys in the promise of Apple. 
Onto sets up 10 percent to roll out so that users' data. The this environment, policy makers should carefully weigh the new look. 
Living it's great to 4 kilobits per gallon it that. Intensive is so remarkable at Bill Graham Civic Auditorium.

#### Topic Title

Them radical, or games on this is no longer quite so publicly. Better from the CAD files, printed one of cable, Apple doesn’t. 
FBI and security protections for new car is on its new. A from the top of our economy and it that users' data. 
Diminished weaken privacy and myriad other avenues like google Fiber is 30 to. Form about 55 mpg impressive, but the unprecedented 
speed has been, first and Austin. Midsize that use end-to-end encryption, and the company’s announcement lacked a single. 
Locks—The the government believes that the fuel efficiency of gaming. Groupe, took to expand into consideration earlier this for 
technical. Cannot example, protecting credit card information when a time when taking. Keys minutes with the potential impact of 
personal data and Showtime.